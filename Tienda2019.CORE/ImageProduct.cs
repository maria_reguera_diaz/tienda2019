﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tienda2019.CORE
{
    /// <summary>
    /// Clase dominio de las imagenes
    /// </summary>
    public class ImageProduct
    {

        /// <summary>
        /// Identificador de la imagen
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Nombre de la ruta de la imagen
        /// </summary>
        public string ImageName { get; set; }
        /// <summary>
        /// Identificador del producto al que pertenece la imagen
        /// </summary>
       [ForeignKey("Product")]
        public int ProductId { get; set; }
        /// <summary>
        /// Colección de producto al que pertence la imagen
        /// </summary>
        public Product Product { get; set; }
        
    }
}
