﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tienda2019.CORE
{
    /// <summary>
    /// Entidad de dirección del usuario.
    /// </summary>
    public class AddressUser
    {
        /// <summary>
        /// Identificador de dirección de usuario.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Calle de usuario
        /// </summary>
        public string Street { get; set; }
        /// <summary>
        /// Numero de la dirección del usuario.
        /// </summary>
        public int Number { get; set; }
        /// <summary>
        /// Piso de la dirección del usuario.
        /// </summary>
        public int Floor { get; set; }
        /// <summary>
        /// Codigo postal de la dirección de usuario.
        /// </summary>
        public int Code { get; set; }
        /// <summary>
        /// Ciudad del usuario.
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// Estado o Provincia de la dirección de usuario.
        /// </summary>
        public string Province { get; set; }
        /// <summary>
        /// Pais del usuario
        /// </summary>
        public string Country { get; set; }
        /// <summary>
        /// Identificador del usuario que ha creado el pedido.
        /// </summary>
        [ForeignKey("User")]
        public string UserId { get; set; }
        /// <summary>
        /// Usuario que ha creado el pedido.
        /// </summary>
        public ApplicationUser User { get; set; }
        

    }
}
