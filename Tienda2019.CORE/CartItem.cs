﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tienda2019.CORE
{
    /// <summary>
    /// Entidad de dominio carrito
    /// </summary>
    public class CartItem
    {
        //public int Id { get; set; }
        ///// <summary>
        ///// Identificador de carrito de la compra
        ///// </summary>
        [Key]
        public string ItemId { get; set; }
        /// <summary>
        /// Cantidad de productos en el carrito
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// Fecha de creación del carrito.
        /// </summary>
        public DateTime DateCreate { get; set; }
        /// <summary>
        /// Identificador del producto al que pertenece el carrito
        /// </summary>
        [ForeignKey("Product")]
        public int ProductId { get; set; }
        /// <summary>
        /// Colección de producto al que pertence el carrito.
        /// </summary>
        public virtual Product Product { get; set; }
        /// <summary>
        /// Identificador de usuario que ha creado el carrito relacionada con CartItem.
        /// </summary>
        [ForeignKey("User")]
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        /// <summary>
        /// Identificador en el que se ve si el carrito tiene usuario registrado.
        /// </summary>
        public string CartId { get; set; }
    }
}
