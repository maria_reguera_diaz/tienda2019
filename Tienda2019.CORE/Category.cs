﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tienda2019.CORE
{
    /// <summary>
    /// Clase dominio de la categoria de los productos
    /// </summary>
    public class Category
    {
        /// <summary>
        /// Identificador de la categoria
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Nombre de la categoria
        /// </summary>
        public string NameCategory { get; set; }
        /// <summary>
        /// Descripcion de la categoria
        /// </summary>
        public string DescriptionCategory { get; set; }
        ///// <summary>
        ///// Colección de productos de una categoria
        ///// </summary>
        public virtual ICollection<Product> Products { get; set; }
    }
}
