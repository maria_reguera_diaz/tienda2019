﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tienda2019.CORE
{
    /// <summary>
    /// Entidad de estado de pedidos
    /// </summary>
    public enum StateOrder
    {
        /// <summary>
        /// Pedido preparacion en curso
        /// </summary>
        Preparado = 0,
        /// <summary>
        /// Pedido enviado.
        /// </summary>
        Enviado = 1,
        /// <summary>
        /// Pedido cancelado
        /// </summary>
        Cancelado = 2,
        /// <summary>
        /// Pedido entregado
        /// </summary>
        Entregado = 3,
    }
}
