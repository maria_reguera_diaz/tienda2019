﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tienda2019.CORE
{
    /// <summary>
    /// Enumeroado del stock de los productos
    /// </summary>
    public enum StockProduct :int
    {
        /// <summary>
        /// Producto Disponible
        /// </summary>
        InStock = 0,
        /// <summary>
        /// Producto agotado
        /// </summary>
        SinStock = 1,
        
    }
}
