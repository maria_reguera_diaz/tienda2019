﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tienda2019.CORE
{
    public class TPV
    {
        /// <summary>
        /// Identificador de la tarjeta
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Numero de la tarjeta.
        /// </summary>
        public string NumberTPV { get; set; }
        /// <summary>
        /// Codigo de seguridad de la tarjeta
        /// </summary>
        public int CVC { get; set; }
        /// <summary>
        /// Fecha de caducidad de la tarjeta.
        /// </summary>
        public DateTime dateExpire { get; set; }
    }
}
