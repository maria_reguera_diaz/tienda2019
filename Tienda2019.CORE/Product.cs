﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tienda2019.CORE
{
    /// <summary>
    /// Clase de dominio del producto
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Identificador de producto.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Identificador de la categoria a la que pertenece el producto.
        /// </summary>
        [ForeignKey("Category")]
        public int CategoryId { get; set; }

        public Category Category { get; set; }

        /// <summary>
        /// Nombre del producto.
        /// </summary>
        public string NameProduct { get; set; }

        /// <summary>
        /// Descripción del producto.
        /// </summary>
        public string DescriptionProduct { get; set; }

        /// <summary>
        /// Precio del producto.
        /// </summary>
        public double PriceUnitProduct { get; set; }

        /// <summary>
        /// Cantidad de producto que queda en stock, solo lo puede ver el administrador.
        /// </summary>
        public int QuantityProduct { get; set; }

        /// <summary>
        /// Estado de stock del producto, 
        /// </summary>
        public StockProduct Stock { get; set; }


        /// <summary>
        /// Listado de Imagenes de un producto. Relación 1 a N
        /// </summary>
        public virtual List<ImageProduct> ImageProducts { get; set; }


        ///// <summary>
        ///// Usuario que ha creado el producto.
        ///// </summary>
        //        public ApplicationUser User { get; set;}
        //[ForeignKey("User")]
        //public string User_Id { get; set; }
        ////TODO:Falta el usuario

    }
}
