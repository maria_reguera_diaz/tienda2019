﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tienda2019.CORE
{
    /// <summary>
    /// Clase linea de pedido.
    /// </summary>
    public class OrderLine
    {
        /// <summary>
        /// Identificador de linea de pedido.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Cantidas de producto en la linea de pedido
        /// </summary>
        public int QuantityProduct { get; set; }
        /// <summary>
        /// Precio unidad de producto
        /// </summary>
        public double PriceUnitProduct { get; set; }

        /// <summary>
        /// Identificador del producto al que pertenece el carrito
        /// </summary>
        [ForeignKey("Product")]
        public int ProductId { get; set; }
        public Product Product { get; set; }
        /// <summary>
        /// Identificador del pedido
        /// </summary>
        [ForeignKey("Order")]
        public int OrderId { get; set; }
        public Order Order { get; set; }

    }
}
