﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tienda2019.CORE
{
    public class Shipment
    {
        /// <summary>
        /// Identificador del envío.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Nombre de la compañia de envio
        /// </summary>
        public string NameShipment { get; set; }
        /// <summary>
        /// Precio del envio
        /// </summary>
        public double PriceShipment { get; set; }
    }
}
