﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tienda2019.CORE
{
    /// <summary>
    /// Entidad de dominio pedidos
    /// </summary>
    public class Order
    {
        /// <summary>
        /// Identificador del pedido.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Fecha de creación del pedido.
        /// </summary>
        public DateTime DateOrder { get; set; }
        /// <summary>
        /// Estado en la que se encuentra el pedido.
        /// </summary>
        public StateOrder StateOrder {get;set;}
        /// <summary>
        /// Importe total del pedido
        /// </summary>
        public double TotalOrder { get; set; }
        /// <summary>
        /// Identificador del usuario que ha creado el pedido.
        /// </summary>
        [ForeignKey("User")]
        public string UserId { get; set; }
        /// <summary>
        /// Usuario que ha creado el pedido.
        /// </summary>
        public ApplicationUser User { get; set; }
        
        /// <summary>
        /// Identificador de la direccion a la que va dirigido el pedido.
        /// </summary>
        [ForeignKey("AddressUser")]
        public int AddressId { get; set; }
        /// <summary>
        /// Dirección a la que va dirigido el pedido.
        /// </summary>
        public AddressUser AddressUser { get; set; }
        /// <summary>
        /// Lista de productos en la linea de pedido.
        /// </summary>
        public List<OrderLine> OrderLines { get; set; }
        /// <summary>
        /// Identificador del método de envio.
        /// </summary>
        [ForeignKey("Shipment")]
        public int ShipmentId { get; set; }
        /// <summary>
        /// Método de envio.
        /// </summary>
        public Shipment Shipment { get; set; }
    }
}
