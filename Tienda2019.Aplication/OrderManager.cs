﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda2019.Application;
using Tienda2019.CORE;
using Tienda2019.DAL;

namespace Tienda2019.Aplication
{
    public class OrderManager : GenericManager<Order>
    {
        public OrderManager(ApplicationDbContext context) : base(context)
        {
        }
  
        /// <summary>
        /// Metodo que obtine los pedidos de los usuarios
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>pedidos de usuarios</returns>
        public IQueryable<Order> GetByUser(string userId)
        {
            return Context.Orders.Where(p => p.UserId == userId);
        }
    }
}
