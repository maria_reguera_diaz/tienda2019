﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda2019.Application;
using Tienda2019.DAL;
using Tienda2019.CORE;

namespace Tienda2019.Aplication
{
    public class OrderLineManager : GenericManager<OrderLine>
    {
        public OrderLineManager(ApplicationDbContext context) : base(context)
        {
        }
        /// <summary>
        /// Metodo que devuelve las lineas de pedidos
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns>Linea de pedido</returns>
        public IQueryable<OrderLine>GetByOrder(int orderId)
        {
            return Context.OrderLines.Where(p => p.OrderId == orderId);
        }
    }
}
