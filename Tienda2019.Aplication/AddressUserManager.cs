﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda2019.Application;
using Tienda2019.CORE;
using Tienda2019.DAL;

namespace Tienda2019.Aplication
{
    /// <summary>
    /// Clase Manager de direcciones de usuario
    /// </summary>
    public class AddressUserManager : GenericManager<AddressUser>
    {
        public AddressUserManager(ApplicationDbContext context) : base(context)
        {
        }
        /// <summary>
        /// Metodo que obtine las direcciones de los usuarios
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>Direccion de usuarios</returns>
        public IQueryable<AddressUser>GetByUser(string userId)
        {
            return Context.AddressUsers.Where(p => p.UserId == userId);
        }
    }
}
