﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda2019.DAL;
using Tienda2019.CORE;


namespace Tienda2019.Application
{
    /// <summary>
    /// Manager de producto
    /// </summary>
    public class ProductManager : GenericManager<Product>
    {
        /// <summary>
        /// Constructor del manager de producto
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public ProductManager(ApplicationDbContext context) : base(context)
        {

        }
        /// <summary>
        /// Selecciona productos por categoria
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns>IQueryable de productos</returns>
        public IQueryable<Product>GetByCategory(int categoryId)
        {
            //Devuelve categoria por productos
            return Context.Products.Where(c => c.CategoryId == categoryId);
        }
        ///// <summary>
        /////Método que retorna la lista de productos de un usuario 
        ///// </summary>
        ///// <param name="User_id">dentificador de usuario</param>
        ///// <returns>Lista de productos</returns>
        //public IQueryable<Product> GetByUserId(string User_id)
        //{
        //    return Context.Products.Where(p => p.User_Id == User_id);

        //}
        ///// <summary>
        ///// Método que retorna un producto por su id y el usuario.
        ///// </summary>
        ///// <param name="id">Identificador de producto</param>
        ///// <param name="idUsuario">Identificador de usuario.</param>
        ///// <returns>producto o null en caso de no existir.</returns>
        //public Product GetbyIdAndUserId(int id, string User_id)
        //{
        //    return Context.Products.Where(p => p.User_Id == User_id && p.Id_Producto == id).SingleOrDefault();
        //}
    }
}
