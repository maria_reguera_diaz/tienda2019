﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Tienda2019.Application;
using Tienda2019.CORE;
using Tienda2019.DAL;

namespace Tienda2019.Aplication
{
    /// <summary>
    /// Clase Manager de acción de carrito
    /// </summary>
    public class CartItemManager : GenericManager<CartItem>
    {
        /// <summary>
        /// Identificador del usuario no registrado en el carrito
        /// </summary>
        public string ShoppingCartId { get; set; }
        public const string CartSessionKey = "CartId";


        /// <summary>
        /// Constructor de Manager de Acción del Carrito
        /// </summary>
        /// <param name="context"></param>
        public CartItemManager(ApplicationDbContext context) : base(context)
        {

        }
        /// <summary>
        /// Metodo que retorna el carrito por usuario registrado.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>Lista de elementos en carrito.</returns>
        public IQueryable<CartItem> GetByUser(String userId)
        {
            return Context.CartItems.Where(p => p.UserId == userId);

        }
        /// <summary>
        /// Metodo que retorna el carrito por su id y el usuario no registrado
        /// </summary>
        /// <param name="id">identificador del carrito</param>
        /// <param name="userId">Identificador del usuario</param>
        /// <returns>Carrito o null en caso de no existir.</returns>
        public CartItem GetByIdAndUserId(string CartId, string userId)
        {
            return Context.CartItems.Where(c => c.UserId == userId && c.CartId == CartId).SingleOrDefault();
        }
        /// <summary>
        /// Metodo que retorna el carrito por usuario y producto
        /// </summary>
        /// <param name="userId">Identificador de usuario</param>
        /// <param name="productId">Identificador de carrito</param>
        /// <returns>Carrito con productos</returns>
        public IQueryable<CartItem> GetByIdAndProduct(string userId, int productId)
        {
            return Context.CartItems.Where(p => p.UserId == userId && p.ProductId == productId);
        }
        /// <summary>
        /// Metodo que devuelve una lista de articulos en el carrito para el usuario no registrado.
        /// </summary>
        /// <param name="userId">Identificador de usuario</param>
        /// <param name="productId">Identificador de producto</param>
        /// <returns>devuelve una lista de articulos en el carrito de la compra para el usuario registrado.</returns>
        public IQueryable<CartItem> GetCartItems(string CartId, int productId)
        {
            return Context.CartItems.Where(c => c.CartId == CartId && c.ProductId == productId);
        }

        /// <summary>
        /// Metodo que devuelve el identificador de la compra para el usuario no registrado
        /// </summary>
        /// <returns></returns>
        public string GetCartId()
        {
            if (HttpContext.Current.Session[CartSessionKey] == null)
            {
                if (!string.IsNullOrWhiteSpace(HttpContext.Current.User.Identity.Name))
                {
                    HttpContext.Current.Session[CartSessionKey] = HttpContext.Current.User.Identity.Name;
                }
                else
                {
                    Guid tempCartId = Guid.NewGuid();
                    HttpContext.Current.Session[CartSessionKey] = tempCartId.ToString();
                }
            }
            return HttpContext.Current.Session[CartSessionKey].ToString();
        }
        /// <summary>
        /// Metodo que incluye los productos en la cesta, si el producto ya está se incrementa en 1
        /// </summary>
        /// <param name="id">Identificador del producto.</param>
        public void AddToCart(int id)
        {
            //Recibe los productos de la base de datos.
            ShoppingCartId = GetCartId();

            var cartItem = Context.CartItems.SingleOrDefault(
                c => c.CartId == ShoppingCartId
                && c.ProductId == id);
            if (cartItem == null)
            {
                //Crea un nuevo carrito si no tiene articulos existentes.
                cartItem = new CartItem
                {
                    ItemId = Guid.NewGuid().ToString(),
                    ProductId = id,
                    CartId = ShoppingCartId,
                    Product = Context.Products.SingleOrDefault(p => p.Id == id),
                    Quantity = 1,
                    DateCreate = DateTime.Now
                };
                Context.CartItems.Add(cartItem);

            }
            else
            {
                // cartItem = new CartItem();
                //si el articulo elegido se encuentra en el carrito, se añade 1.
                cartItem.Quantity++;
            }

            Context.SaveChanges();
        }

        public void Dispose()
        {
            if (Context != null)
            {
                Context.Dispose();
                // Context = null;
            }
        }
        /// <summary>
        /// Metodo que devuelve una lista de articulos en la cesta de la compra para el usuario.
        /// </summary>
        /// <returns></returns>
        public List<CartItem> GetCartItems()
        {
            ShoppingCartId = GetCartId();
            return Context.CartItems.Where(c => c.CartId == ShoppingCartId).ToList();
        }

        public decimal GetTotal()
        {
            ShoppingCartId = GetCartId();
            //Multiplica el precio del producto por la cantidad de productos que se encuentran en el carrito 
            //y así conocer el precio de la compra. 
            //Suma todos los precios y cantidades de productos para obtener el total del carrito.
            decimal? total = decimal.Zero;
            total = (decimal?)(from cartItems in Context.CartItems
                               where cartItems.CartId == ShoppingCartId
                               select (int?)cartItems.Quantity *
                               cartItems.Product.PriceUnitProduct).Sum();
            return total ?? decimal.Zero;
        }
        /// <summary>
        /// Método que hace la suma de los articulos que hay en el carrito
        /// </summary>
        /// <returns></returns>
        public int GetCount()
        {
            ShoppingCartId = GetCartId();

            // Hace la suma de los artículos que están en el carrito.         
            int? count = (from cartItems in Context.CartItems
                          where cartItems.CartId == ShoppingCartId
                          select (int?)cartItems.Quantity).Sum();
            // Devuelve 0 si no hay articulos en el carrito.       
            return count ?? 0;
        }
    }
}


