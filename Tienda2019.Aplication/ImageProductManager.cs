﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda2019.DAL;
using Tienda2019.CORE;



namespace Tienda2019.Application
{
    public class ImageProductManager : GenericManager<ImageProduct>
    {
        public ImageProductManager(ApplicationDbContext context) : base(context)
        {

        }
        /// <summary>
        /// Selecciona imágenes de productos
        /// </summary>
        /// <param name="id"></param>
        /// <returns>IQueryable de imágenes</returns>
        public IQueryable<ImageProduct>GetByProduct(int id)
        {
            return Context.ImageProducts.Where(p => p.ProductId == id);
        }
    }
}
