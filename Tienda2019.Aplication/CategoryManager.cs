﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda2019.DAL;
using Tienda2019.CORE;


namespace Tienda2019.Application
{
    public class CategoryManager : GenericManager<Category>
    {
        /// <summary>
        /// Constructor de la clase Manager Category
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public CategoryManager(ApplicationDbContext context) : base(context)
        {

        }
        //public IQueryable<Category> GetByUserId(string userId)
        //{
        //    return Context.Set<Category>().Where(e => e.User_Id == userId);
        //}
    }
}
