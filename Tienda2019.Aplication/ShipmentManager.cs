﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda2019.Application;
using Tienda2019.CORE;
using Tienda2019.DAL;

namespace Tienda2019.Aplication
{
    /// <summary>
    /// Clase Manager de Envios
    /// </summary>
    public class ShipmentManager : GenericManager<Shipment>
    {
        public ShipmentManager(ApplicationDbContext context) : base(context)
        {
        }
    }
}
