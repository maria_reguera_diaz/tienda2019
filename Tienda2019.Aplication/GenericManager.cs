﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda2019.DAL;


namespace Tienda2019.Application
{
    /// <summary>
    /// Clase genérica de Manager
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class GenericManager<T>
        where T: class
    {
        /// <summary>
        /// Contexto de datos del manager
        /// </summary>
        public ApplicationDbContext Context { get; private set; }
        /// <summary>
        /// Constructor del manager generico.
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public GenericManager(ApplicationDbContext context)
        {
            Context = context;
        }
        /// <summary>
        /// Añade una entidad al contexto de datos
        /// </summary>
        /// <param name="entity">Entidad a añadir</param>
        /// <returns>Entidad añadida</returns>
        public T Add(T entity)
        {
            return Context.Set<T>().Add(entity);
        }
        /// <summary>
        /// Elimina una entidad del contexto de datos
        /// </summary>
        /// <param name="entity">Entidad a eliminar</param>
        /// <returns>Entidad eliminada</returns>
        public T Remove(T entity)
        {
            return Context.Set<T>().Remove(entity);
        }
        /// <summary>
        /// Obtiene una entidad por sus claves
        /// </summary>
        /// <param name="key">Claves del objeto</param>
        /// <returns>Entidad si es encontrada</returns>
        public T GetById(object[]key)
        {
            return Context.Set<T>().Find(key);
        }
        /// <summary>
        /// Obtiene una entidad por su clave 
        /// </summary>
        /// <param name="id">Identificador</param>
        /// <returns>Entidad si es encontrada</returns>
        public T GetById(int id)
        {
            return GetById(new object[] { id });
        }
        /// <summary>
        /// Obtiene todas las entidades de un tipo específico.
        /// </summary>
        /// <returns></returns>
        public IQueryable<T> GetAll()
        {
            return Context.Set<T>();
        }

    }
}
