﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ShoppingCart.aspx.cs" Inherits="Tienda2019.Web.ShoppingCart.ShoppingCart" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!--Cargamos los css y js-->
    <link href="../Content/propio.css" rel="stylesheet" />
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <script src="../Scripts/bootstrap.min.js"></script>
    <script src="../Scripts/jquery-1.12.4.min.js"></script>
      <div id="lblInfo" runat="server" class="ContentHead">
    <h3>Carrito de la compra</h3>
  </div>
    <br />
    
    
    <asp:GridView ID="gvCartList" runat="server" AutoGenerateColumns="False" CellPadding="4" CssClass="table table-striped table-bordered" GridLines="Vertical" ItemType="Tienda2019.CORE.CartItem" SelectMethod="GetShoppingCart" ShowFooter="True">
        <Columns>
            <asp:BoundField DataField="Product.Id" HeaderText="ID" />
            <asp:BoundField DataField="Product.NameProduct" HeaderText="Nombre" />
            <asp:BoundField DataField="Product.PriceUnitProduct" HeaderText="Precio unidad" DataFormatString="{0:c}" />
            <asp:TemplateField HeaderText="Cantidad">
                <ItemTemplate>
                    <asp:TextBox ID="txtQuantity" runat="server" Width="40" Text="<%#: Item.Quantity %>"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>
 
            <asp:TemplateField HeaderText="Total">
                <ItemTemplate>
                    <%#: String.Format("{0:c}",((Convert.ToDouble(Item.Quantity))*Convert.ToDouble(Item.Product.PriceUnitProduct))) %>
                </ItemTemplate>
                </asp:TemplateField>
 
            <asp:TemplateField HeaderText="Quitar del carrito">
                <ItemTemplate>
                    <asp:Button ID="btnRemove" runat="server" CssClass="btn btn-danger btn-sm" OnClick="btnRemove_Click" /><i class="fa fa-trash-o" ></i>              
                </ItemTemplate>
                </asp:TemplateField>
               
        </Columns>
    </asp:GridView>
    <div>
        <p> </p>
            <strong>
                <asp:Label ID="lblTotalText" runat="server" Text="Total Pedido: "></asp:Label>
                <asp:Label ID="lblTotal" runat="server" EnableViewState="false" Text=""></asp:Label>
            </strong>
    </div>
    <br />
    <table>
        <tr>
           <td><a href="../Catalogo/Catalogue.aspx" class="btn btn-warning"><i class="fa fa-angle-left "></i>  Continúe Comprando</a></td> 
        </tr>
    </table>
    
    <br />
   
 
</asp:Content>
