﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tienda2019.Aplication;
using Tienda2019.DAL;

namespace Tienda2019.Web.ShoppingCart
{
    public partial class RemoveCart : System.Web.UI.Page
    {

        ApplicationDbContext context = null;
        CartItemManager cartItemManager = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            cartItemManager = new CartItemManager(context);
      

            if (Request.QueryString["Remove"] != null)
            {
                string cartId = Request.QueryString["Remove"];

                var item = cartItemManager.GetById(int.Parse(cartId));
                cartItemManager.Remove(item);
                cartItemManager.Context.SaveChanges();
                
            }
            Response.Redirect("ShoppingCart/ShoppingCart.aspx");
        }
    }
}