﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Providers.Entities;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tienda2019.Aplication;
using Tienda2019.Application;
using Tienda2019.CORE;
using Tienda2019.DAL;
using Tienda2019.Web.Models;

namespace Tienda2019.Web.ShoppingCart
{
    public partial class ShoppingCart : System.Web.UI.Page
    {

        ApplicationDbContext context = null;
        CartItemManager cartItemManager = null;
        //ProductManager productManager = null;

        /// <summary>
        /// Muestra todos los elementos que el usuario añade a el carrito.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            cartItemManager = new CartItemManager(context);
            //productManager = new ProductManager(context);

            decimal cartTotal = 0;
            cartTotal = cartItemManager.GetTotal();
            if (cartTotal > 0)
            {
                lblTotal.Text = String.Format("{0:c}", cartTotal);
            }
            else
            {
                lblTotalText.Text = "";
                lblTotal.Text = "";
                lblInfo.InnerText = "La cesta está vacia";
                //btnUpdate.Visible = false;
            }
        }

        /// <summary>
        /// Devuelve lista de objetos en el carrito de la compra.
        /// </summary>
        /// <returns></returns>
        public List<CartItem> GetShoppingCart()
        {
            context = new ApplicationDbContext();
            cartItemManager = new CartItemManager(context);
            //productManager = new ProductManager(context);

            return cartItemManager.GetCartItems();

        }
       

       
        /// <summary>
        /// Evento que quita un articulo del carrito de la compra.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemove_Click(object sender, EventArgs e)
        {
            Response.Redirect("RemoveCart.aspx?ItemId={0}'");

            //String.Format("Cart({0})"
        }
    }
}