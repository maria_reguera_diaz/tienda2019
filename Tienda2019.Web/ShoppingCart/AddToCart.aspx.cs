﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tienda2019.Aplication;
using Tienda2019.Application;
using Tienda2019.CORE;
using Tienda2019.DAL;

namespace Tienda2019.Web.ShoppingCart
{
    /// <summary>
    /// Clase que añade el producto seleccionado a la cesta de la compra. Clase puente
    /// </summary>
    public partial class AddToCart : System.Web.UI.Page
    {

        ApplicationDbContext context = null;
        CartItemManager cartItemManager = null;
        //CartItem cartItem = null;
        /// <summary>
        /// Metodo que devuelve el seguimiento de los artículos guardados en la cesta de la compra, 
        /// cuando el usuario no se ha registrado.
        /// </summary>
        /// <returns></returns>



        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            cartItemManager = new CartItemManager(context);


            //convertimos el identificador
            string rawId = Request.QueryString["ProductId"];
            int productoId;


            if (!String.IsNullOrEmpty(rawId) && int.TryParse(rawId, out productoId))
            {
                cartItemManager.AddToCart(Convert.ToInt16(rawId));
                //cartItemManager.Context.SaveChanges();
                //cartItemManager.AddToCart(id);
            }

            else
            {
                Debug.Fail("ERROR : Nunca deberiamos haber un Carrito sin un Identificador de Producto");
                throw new Exception("ERROR : Es ilegal cargar addtocart.aspx sin configurar un ID de producto");
            }
            Response.Redirect("~/ShoppingCart/ShoppingCart.aspx");
        }
    }
}