﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Tienda2019.Web.Startup))]
namespace Tienda2019.Web
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
