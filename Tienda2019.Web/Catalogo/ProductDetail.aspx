﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProductDetail.aspx.cs" Inherits="Tienda2019.Web.Catalogo.ProductDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="../Content/bootstrap.css" rel="stylesheet" />
    <link href="../Content/productDetails.css" rel="stylesheet" />
    <link href="../Content/tienda2019.css" rel="stylesheet" type="text/css" >
    <link href="../Content/maria.css" rel="stylesheet" type="text/css" >

    <div class="container">
        <div class="card">
            <div class="container-fluid">
                <div class="wrapper row">
                    <div class="previe"> <!--preview-pic tab-content   preview-thumbnail nav nav-tabs-->
                        <div class="preview-pic tab-content"><!--Imagen en contendor grande-->
                        <asp:Image ID="ImageProductUrl" runat="server" />
                            
                             <div class="tab-pane active" id="pic-1"><img src="../ImagenCatalogos/Big/" /></div>
						  <div class="tab-pane" id="pic-2"><img src="../ImagenCatalogos/Big/" /></div>
                            </div>
                        <br />
                        <ul class="preview-thumbnail nav nav-tabs"><!--Conjunto de imagenes-->
                             <li class="active"><a data-target="#pic-1" data-toggle="tab"><img src="../ImagenCatalogos/Big/" /></a></li>
						  <li><a data-target="#pic-2" data-toggle="tab"><img src="../ImagenCatalogos/Big/" /></a></li>
                        </ul>
                    </div>
                    <div class="product-content">                         
                    <h2>
                        <asp:Label ID="txtNameProduct" runat="server" Text=""></asp:Label></h2>
                    <h5>
                        <asp:Label ID="txtDescriptionProduct" runat="server" Text=""></asp:Label>
                    </h5>
                    <h4>
                        <asp:Label ID="txtPriceUnitProduct" runat="server" Text=""></asp:Label></h4>
                        <h5>
                            <asp:Label ID="txtQuantityProduct" runat="server" Text=""></asp:Label>
                        </h5>
                    <div class="action">
                        <asp:Button ID="btnAddToCart" runat="server" Text="Al carrito" Cssclass="btn btn-outline-secondary" OnClick="btnAddToCart_Click" />
                        <a href="../Catalogo/Catalogue.aspx" class="btn btn-toolbar" runat="server">Volver a la Tienda</a>
                    </div>
                </div>
        </div>
    </div>
    </div>

  	
</asp:Content>
