﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tienda2019.Application;
using Tienda2019.CORE;
using Tienda2019.DAL;

namespace Tienda2019.Web.Catalogo
{
    public partial class ProductDetail : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        ProductManager productManager = null;
        ImageProductManager imageProductManager = null;
        Product product = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            productManager = new ProductManager(context);
            imageProductManager = new ImageProductManager(context);



            var productId = Request.QueryString["productId"];
            if (productId != null)
            {
                int productoId = 0;
                if (int.TryParse(productId, out productoId))
                {
                    product = productManager.GetById(productoId);
                    if (product != null)
                    {

                        var imageProducts = imageProductManager.GetAll()
           .Include(p => p.Product)
           .Where(p => p.ProductId == product.Id);

                        if (imageProducts != null)
                        {

                            foreach (var imageProduct in imageProducts)

                            {
                                ImageProductUrl.ImageUrl = "~/ImagenCatalogos/Big/" + imageProduct.ImageName;

                            }
                        }

                    }
                    txtNameProduct.Text = product.NameProduct;
                    txtDescriptionProduct.Text = product.DescriptionProduct;
                    txtPriceUnitProduct.Text = product.PriceUnitProduct.ToString("C");
                    txtQuantityProduct.Text = product.QuantityProduct.ToString();

                    //Se mostrará si el producto está en stock o no.
                    if (product.QuantityProduct < 1)
                    {
                        txtQuantityProduct.Text = "Agotado";
                    }
                    else if (product.QuantityProduct < 10)
                    {
                        txtQuantityProduct.Text = "En Stock";
                    }
                }
            }
        }
        protected void btnAddToCart_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ShoppingCart/AddToCart.aspx?productId=" + product.Id);
        }
    }
}