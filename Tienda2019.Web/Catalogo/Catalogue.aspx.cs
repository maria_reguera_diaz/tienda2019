﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tienda2019.Application;
using Tienda2019.CORE;
using Tienda2019.DAL;
using Tienda2019.Web.Models;
using Tienda2019.Web.Controls;
using System.Web.UI.HtmlControls;

namespace Tienda2019.Web.Catalogo
{
    public partial class Catalogue : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        ProductManager productManager = null;
        Product product = null;
        CategoryManager categoryManager = null;
        ImageProductManager imageProductManager = null;
        //Category category = null;
        // string categoryId;
        //List<ImageProduct> list = new List<ImageProduct>();
        

        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            productManager = new ProductManager(context);
            product = new Product();
            categoryManager = new CategoryManager(context);
            imageProductManager = new ImageProductManager(context);

            string categoryId = Request.QueryString["Id"];
            var categories = categoryManager.GetAll();

            //TODO: selección producto desde el DropDownList de categorias

            if (ddlNameCategory.SelectedItem == null)
            {
                foreach (var category in categories)
                {
                    ListItem listItem = new ListItem(category.NameCategory, category.Id.ToString());
                    ddlNameCategory.Items.Insert(ddlNameCategory.Items.Count, listItem);
                }
            }
            string productId;
            productId = Request.QueryString["id"];
            {
                if(categoryId!=null)
                {
                    var products = productManager.GetByCategory(int.Parse(categoryId))
                        .Include(p => p.ImageProducts);
                    foreach (var product in products)
                    {
                        LoadProduct(product);
                    }

                }
                else
                {
                    var products = productManager.GetAll().Include(p => p.ImageProducts);
                    foreach (var product in products)
                    {
                        LoadProduct(product);
                    }
                }
            }
 
        }

/// <summary>
/// Metodo para cargar la página de control
/// </summary>
/// <param name="product"></param>
    private void LoadProduct(Product product)
        {
            //de la pagina principal va a Maincontent
            //var content = (ContentPlaceHolder)Master.FindControl("MainContent");
            //buscar el div de mensaje
            var div = products;
            
             
            foreach (var imageProduct in product.ImageProducts)
            {
                //nos devuelve un control
                var control = (CatalogueProductControl)Page.LoadControl("~/Controls/CatalogueProductControl.ascx");
                control.Product = product;
                div.Controls.Add(control);
            }
        }
    protected void btnAddCart_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ShoppingCart/AddToCart.aspx?Id=" + product.Id);
        }
    }
}

