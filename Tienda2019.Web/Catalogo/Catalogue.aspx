﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Catalogue.aspx.cs" Inherits="Tienda2019.Web.Catalogo.Catalogue" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!-- Bootstrap core CSS -->
    <link href="~/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../Content/CatalogueProduct.css" rel="stylesheet" />
    <!-- Custom styles for this template -->

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="Scripts/jquery-1.8.0.min.js"></script>
    <script src="Scripts/bootstrap-select.min.js"></script>
    <link href="../Content/bootstrap-select.min.css" rel="stylesheet" />
    <link href="../Content/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/propio.css" rel="stylesheet" />
	<link href="../Content/bootstrap.css" rel="stylesheet" type="text/css" >
	<link href="../Content/tienda2019.css" rel="stylesheet" type="text/css" >

     <!--Contenido de la pagina-->
    <div class="container">
       <br />
        <!--Filas donde va el contenido de la pagina-->
        <div class="row">
            <!--Fila lateral donde colocamos las categorias-->
            <!--<div class="col-md-3">-->
                <h2>Tienda 2019</h2>
               
                <!--Listado de las categorias-->
                <!--<div class="col-md-10">-->
                <div class="list-group">
                    <asp:DropDownList  ID="ddlNameCategory" runat="server" CssClass="selectpcker" dat-style="btn-primary" ></asp:DropDownList>
                </div>
                    </div>      
            <!--Fila donde colocamos el resto del contenido-->
           <!-- <div class="col-lg-9">-->
                
               
<!--colocar el catalogueProductControl-->
                
                    <div class="contendor" id="products" runat="server"></div>
           
                <!-- /.col-lg-9 -->
            <!--</div>-->
            
            <!--div de CatalogueProductControl-->
        </div>      
    <!-- Custom styles for this template -->
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</asp:Content>
