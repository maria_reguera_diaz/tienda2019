﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tienda2019.Application;
using Tienda2019.DAL;

namespace Tienda2019.Web.Admin
{
    /// <summary>
    /// Listado de categorías.
    /// </summary>
    public partial class ListCategory : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        CategoryManager categoryManager = null;



        protected void Page_Load(object sender, EventArgs e)
        {
            //Conecta con la base de datos.
            context = new ApplicationDbContext();
            categoryManager = new CategoryManager(context);


            var listCategory = categoryManager.GetAll().ToList();

            string formatLinkEdit = "<a class='btn btn-primary' href='EditCategory.aspx?Id={0}'>Modificar</a>";
            string formatLinkDelete = "<a class='btn btn-danger' href='RemoveCategory.aspx?Id={0}'>Eliminar</a>";
           

            foreach (var category in listCategory)
            {
                var row = new TableRow();
                row.Cells.Add(new TableCell() { Text = category.Id.ToString() });
                row.Cells.Add(new TableCell() { Text = category.NameCategory.ToString() });
                row.Cells.Add(new TableCell() { Text = category.DescriptionCategory.ToString() });
                row.Cells.Add(new TableCell() { Text = string.Format(formatLinkEdit, category.Id) });
                row.Cells.Add(new TableCell() { Text = string.Format(formatLinkDelete, category.Id)});
                   

                tbodyListCategory.Controls.Add(row);
                //TODO: convertir id de category a nombre
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddCategory.aspx");
        }

        protected void btnIndex_Click(object sender, EventArgs e)
        {
            Response.Redirect("Index.aspx");
        }
    }
}