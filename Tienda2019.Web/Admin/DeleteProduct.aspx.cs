﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tienda2019.Application;
using Tienda2019.DAL;

namespace Tienda2019.Web.Admin
{
    /// <summary>
    /// Entidad que elimina los productos de la base de datos.
    /// </summary>
    public partial class DeleteProduct : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        ProductManager productManager = null;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            productManager = new ProductManager(context);

            var productId = Request.QueryString["Id"];
            if(productId != null)
            {
                var product = productManager.GetById(int.Parse(productId));
                productManager.Remove(product);
                context.SaveChanges();
                Response.Redirect("ListProduct.aspx");
            }
        }
    }
}