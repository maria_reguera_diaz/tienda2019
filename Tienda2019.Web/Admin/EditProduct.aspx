﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditProduct.aspx.cs" Inherits="Tienda2019.Web.Admin.EditProduct" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="form-horizontal">
            <h3>Modificar Producto</h3>
            <asp:HiddenField ID="txtId" runat="server" />
            <div class="form-group">

                <div class="form-group.internal">
                    <br />

                    <asp:Label ID="lblNameCategory" runat="server" CssClass="col-md-3.col-me-offset-2 control-label" Text="Categoria" AssociatedControlID="txtNameProduct"></asp:Label>
                    <asp:DropDownList ID="ddlNameCategory" runat="server" CssClass="form-control"></asp:DropDownList>


                    <asp:Label ID="lblNameProduct" runat="server" CssClass="col-md-3.col-me-offset-2 control-label" Text="Nombre" AssociatedControlID="txtNameProduct"></asp:Label>
                    <asp:TextBox ID="txtNameProduct" runat="server" CssClass="form-control" />



                    <asp:Label ID="lblPriceUnitProduct" runat="server" CssClass="col-md-3.col-me-offset-2 control-label" Text="Precio Unidad" AssociatedControlID="txtPriceUnitProduct"></asp:Label>
                    <asp:TextBox ID="txtPriceUnitProduct" runat="server" CssClass="form-control" />


                    <asp:Label ID="lblQuantityProduct" runat="server" CssClass="col-md-3.col-me-offset-2 control-label" Text="Cantidad" AssociatedControlID="txtQuantityProduct"></asp:Label>
                    <asp:TextBox ID="txtQuantityProduct" runat="server" CssClass="form-control" />
                </div>
            </div>

            <div class="form-horizontal">
                <div class="form-group">

                    <asp:Label ID="lblDescriptionProduct" runat="server" CssClass="col-md-2.col-md-2 control-label" Text="Descripción" AssociatedControlID="txtDescriptionProduct"></asp:Label>
                    <asp:TextBox ID="txtDescriptionProduct" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="8"></asp:TextBox>
                    <asp:Label ID="lblImageProduct" runat="server" CssClass="control-label" Text="Imagen" AssociatedControlID="fuImageProduct"></asp:Label>
                    <asp:FileUpload ID="fuImageProduct" runat="server" AllowMultiple="true" CssClass="form-control" />
                    <br />
                    <asp:Image ID="ImageProductUrl" runat="server" Style="height: 200px; width: 200px;" CssClass="form-control" />

                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">

                    <asp:Button ID="btnUpdate" runat="server" Text="Modificar" CssClass="btn btn-success" OnClick="btnUpdate_Click" />
                    <asp:Button ID="btnVolver" runat="server" Text="Volver a listado" CssClass="btn btn-warning" OnClick="btnVolver_Click" />
                    <asp:Button ID="btnIndex" runat="server" Text="Panel de administracion" style="margin-left:10px" CssClass="btn btn-primary" OnClick="btnIndex_Click" />
                     
                </div>
            </div>
            <asp:Label ID="lblResult" runat="server"></asp:Label>
            <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>

        </div>
    </div>
</asp:Content>
