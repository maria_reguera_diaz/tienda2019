﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddProduct.aspx.cs" Inherits="Tienda2019.Web.Admin.AddProduct" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container">
        <br />
        <h4>Añadir producto</h4>

                        <asp:Label ID="lblNameCategory" runat="server" Text="Categoria" AssociatedControlID="txtNameProduct"></asp:Label>
                        <asp:DropDownList ID="ddlNameCategory" runat="server" CssClass="form-control"></asp:DropDownList>
     
                        <asp:Label ID="lblNameProduct" runat="server" Text="Nombre" AssociatedControlID="txtNameProduct"></asp:Label>
                        <asp:TextBox ID="txtNameProduct" runat="server" CssClass="form-control" />

                        <asp:Label ID="lblPriceUnitProduct" runat="server" Text="Precio Unidad" AssociatedControlID="txtPriceUnitProduct"></asp:Label>
                        <asp:TextBox ID="txtPriceUnitProduct" runat="server" CssClass="form-control" />

                        <asp:Label ID="lblQuantityProduct" runat="server" Text="Cantidad" AssociatedControlID="txtQuantityProduct"></asp:Label>
                        <asp:TextBox ID="txtQuantityProduct" runat="server" CssClass="form-control" />


                        <asp:Label ID="lblDescriptionProduct" runat="server" Text="Descripción" AssociatedControlID="txtDescriptionProduct"></asp:Label>
                        <asp:TextBox ID="txtDescriptionProduct" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="8"></asp:TextBox>
                        <asp:Label ID="lblImageProduct" runat="server" Text="Imagen" AssociatedControlID="fuImageProduct"></asp:Label>
                        <asp:FileUpload ID="fuImageProduct" runat="server" AllowMultiple="true" CssClass="form-control btn btn-primary" />
                  
        <br />
                        <asp:Button ID="btnSave" runat="server" Text="Guardar" CssClass="btn btn-success" OnClick="btnSave_Click" />
                        <asp:Button ID="btnVolver" runat="server" Text="Volver a listado" CssClass="btn btn-warning" OnClick="btnVolver_Click" />
                        <asp:Button ID="btnIndex" runat="server" Text="Panel de administracion" style="margin-left:10px" CssClass="btn btn-primary" OnClick="btnIndex_Click" />

        <asp:Label ID="lblResult" runat="server"></asp:Label>
        <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
    </div>
</asp:Content>
