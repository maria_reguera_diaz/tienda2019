﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListUser.aspx.cs" Inherits="Tienda2019.Web.Admin.ListUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div style="margin-top:10px">
        <asp:Button ID="btnNew" runat="server" Text="Nuevo producto" CssClass="btn btn-primary" OnClick="btnNew_Click" />
        <asp:Button ID="btnIndex" runat="server" Text="Panel de administracion" style="margin-left:10px" CssClass="btn btn-primary" OnClick="btnIndex_Click" />
    </div>
    <br />
    <table id="tbListUser">
        <tr>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Calle</th>
            <th>Nº</th>
            <th>Piso</th>
            <th>C.P.</th>
            <th>Ciudad</th>
            <th>Provincia</th>
            <th>Paí</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        <tbody id="tBodyListUser"></tbody>
    </table>
     <%-- Cargamos el js datatable script--%>
    <script src="<%: ResolveUrl("~/Scripts/DataTables/media/js/jquery.dataTables.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tbListUser').DataTable({
                "languaje": {
                    "url": "//cdn.datatables.net/plug ins/1.10.13/i18n/Spanish.json"
                }
            });
        });
    </script>

</asp:Content>
