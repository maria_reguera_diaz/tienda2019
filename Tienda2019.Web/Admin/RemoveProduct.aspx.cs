﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tienda2019.Application;
using Tienda2019.DAL;

namespace Tienda2019.Web.Admin
{
    public partial class RemoveProduct : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        ProductManager productManager = null;
        CategoryManager categoryManager = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            productManager = new ProductManager(context);
            categoryManager = new CategoryManager(context);

            string productId = Request.QueryString["Id"];
            var products = categoryManager.GetAll();

            if (productId != null)
            {
                //Cargamos la categoria en el formulario de edición
                var product = categoryManager.GetById(int.Parse(productId));
                categoryManager.Remove(product);
                categoryManager.Context.SaveChanges();
                Response.Redirect("ListProduct.aspx");
            }
        }
    }
}