﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tienda2019.Application;
using Tienda2019.CORE;
using Tienda2019.DAL;

namespace Tienda2019.Web.Admin
{
    /// <summary>
    /// Clase que Elimina una categoria de la base de datos.
    /// </summary>
    public partial class RemoveCategory : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        CategoryManager categoryManager = null;
        protected void Page_Load(object sender, EventArgs e)
        {

            context = new ApplicationDbContext();
            categoryManager = new CategoryManager(context);

            string categoryId = Request.QueryString["Id"];
            var categories = categoryManager.GetAll();

            if (categoryId != null)
            {
                //Cargamos la categoria en el formulario de edición
                var category = categoryManager.GetById(int.Parse(categoryId));
                categoryManager.Remove(category);
                categoryManager.Context.SaveChanges();
                Response.Redirect("ListCategory.aspx");
            }
        }
    }
}