﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditCategory.aspx.cs" Inherits="Tienda2019.Web.Admin.EditCategory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <h4>Editar categorías</h4>
        <div class="form-group">
            <asp:Label ID="lblTxt" Text="" runat="server"></asp:Label>
            <br />
            <asp:Label ID="lblNameCategory" runat="server" CssClass="col-md-2.col-me-offset-2 control-label" Text="Nombre" AssociatedControlID="txtNameCategory"></asp:Label>
            <asp:TextBox ID="txtNameCategory" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label ID="lblDescriptionCategory" runat="server" CssClass="col-md-2.col-md-offset-2 control-label" Text="Descripción" AssociatedControlID="txtDescriptionCategory"></asp:Label>
            <asp:TextBox ID="txtDescriptionCategory" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="8"></asp:TextBox>
        </div>
        <asp:Button ID="btnSave" runat="server" Text="Guardar" CssClass="btn btn-success" OnClick="btnSave_Click"  />
        <asp:Button ID="btnVolver" runat="server" Text="Volver a listado" CssClass="btn btn-warning" OnClick="btnVolver_Click" />
        <asp:Button ID="btnIndex" runat="server" Text="Panel de administracion" style="margin-left:10px" CssClass="btn btn-primary" OnClick="btnIndex_Click" />
        <asp:Label ID="lblResult" runat="server"></asp:Label>
    </div>
</asp:Content>
