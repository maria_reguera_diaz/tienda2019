﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tienda2019.Application;
using Tienda2019.CORE;
using Tienda2019.DAL;

namespace Tienda2019.Web.Admin
{
    /// <summary>
    /// Listado de productos que verá el administrador.
    /// </summary>
    public partial class ListProduct : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        ProductManager productManager = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Conecta con la base de datos.
            context = new ApplicationDbContext();
            productManager = new ProductManager(context);




            var listProduct = productManager.GetAll().ToList();

            string formatLinkEdit = "<a class='btn btn-primary' href='EditProduct.aspx?Id={0}'>Modificar</a>";
            string formatLinkDelete = "<a class='btn btn-danger' href='RemoveProduct.aspx?Id={0}'>Eliminar</a>";

            Category category = new Category();
            category = null;




            foreach (var product in listProduct)
            {
                var row = new TableRow();
                row.Cells.Add(new TableCell() { Text = product.Id.ToString() });
                row.Cells.Add(new TableCell() { Text = product.NameProduct.ToString() });
                row.Cells.Add(new TableCell() { Text = product.DescriptionProduct.ToString() });
                row.Cells.Add(new TableCell() { Text = product.PriceUnitProduct.ToString() });
                row.Cells.Add(new TableCell() { Text = product.QuantityProduct.ToString() });
                row.Cells.Add(new TableCell() { Text = product.CategoryId.ToString() });
                row.Cells.Add(new TableCell() { Text = string.Format(formatLinkEdit, product.Id) });
                row.Cells.Add(new TableCell() { Text = string.Format(formatLinkDelete, product.Id) });


                tBodyListProduct.Controls.Add(row);
                //TODO: convertir id de categoria a nombre
            }
        }
        /// <summary>
        /// Evento que te lleva a la ventana añadir productos.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddProduct.aspx");
        }
        /// <summary>
        /// Evento que te lleva a el panel del administrador.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnIndex_Click(object sender, EventArgs e)
        {
            Response.Redirect("Index.aspx");
        }
    }
}
