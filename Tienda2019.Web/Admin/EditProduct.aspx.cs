﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tienda2019.Application;
using Tienda2019.CORE;
using Tienda2019.DAL;

namespace Tienda2019.Web.Admin
{
    /// <summary>
    /// Clase que edita los productos añadidos.
    /// </summary>
    public partial class EditProduct : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        ProductManager productManager = null;
        CategoryManager categoryManager = null;
        ImageProductManager imageProductManager = null;

        /// <summary>
        /// Visualiza el producto elegido
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            context = new ApplicationDbContext();
            productManager = new ProductManager(context);
            categoryManager = new CategoryManager(context);
            imageProductManager = new ImageProductManager(context);

            string productId = Request.QueryString["Id"];
            var categories = categoryManager.GetAll();

            if (ddlNameCategory.SelectedItem == null)
            {
                foreach (var category in categories)
                {
                    ListItem listItem = new ListItem(category.NameCategory, category.Id.ToString());
                    ddlNameCategory.Items.Insert(ddlNameCategory.Items.Count, listItem);
                }
            }

            if (productId != null)
            {

                var product = productManager.GetById(int.Parse(productId));
                var category = categoryManager.GetById(product.CategoryId);


                if (!Page.IsPostBack)
                {
                    ddlNameCategory.SelectedValue = category.Id.ToString();
                    txtNameProduct.Text = product.NameProduct;
                    txtPriceUnitProduct.Text = product.PriceUnitProduct.ToString();
                    txtQuantityProduct.Text = product.QuantityProduct.ToString();
                    txtDescriptionProduct.Text = product.DescriptionProduct;


                    var imageProducts = imageProductManager.GetAll();
                    if (imageProducts != null)
                    {
                        foreach (var imageProduct in imageProducts)
                        {
                            ImageProductUrl.ImageUrl = "~/ImagenCatalogos/Big/" + imageProduct.ImageName;
                        }
                    }

                    
                }
            }
        }
        /// <summary>
        /// Actualiza el producto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                string productId = Request.QueryString["Id"];
                if (productId != null)
                {
                    Product product = new Product();
                    product = productManager.GetById(int.Parse(productId));

                    product.CategoryId = int.Parse(ddlNameCategory.SelectedValue);
                    product.NameProduct = txtNameProduct.Text;
                    product.DescriptionProduct = txtDescriptionProduct.Text;
                    product.PriceUnitProduct = double.Parse(txtPriceUnitProduct.Text);
                    product.QuantityProduct = int.Parse(txtQuantityProduct.Text);
                    product.ImageProducts = new List<ImageProduct>();



                    if (fuImageProduct.HasFile)
                    {
                        foreach (var file in fuImageProduct.PostedFiles)
                        {
                            imageProductManager = new ImageProductManager(context);
                            String fileExtension = System.IO.Path.GetExtension(file.FileName).ToLower();
                            String[] allowedExtension = { ".png", ".jpg", ".jpeg", ".gif" };
                            if (allowedExtension.Contains(fileExtension))
                            {
                                file.SaveAs(Server.MapPath("~/ImagenCatalogos/Big/") + file.FileName);
                            }
                            ImageProduct imageProduct = new ImageProduct()
                            {
                                ImageName = file.FileName,
                                ProductId = product.Id
                            };
                            product.ImageProducts.Add(imageProduct);
                            imageProductManager.Add(imageProduct);
                            imageProduct = null;
                        }
                        imageProductManager.Context.SaveChanges();
                    }
                    var imageProducts = imageProductManager.GetAll();
                    if (imageProducts != null)
                    {
                        foreach (var imageProduct in imageProducts)
                        {
                            ImageProductUrl.ImageUrl = "~/ImagenCatalogos/Big/" + imageProduct.ImageName;
                        }
                    }
                    //productManager.Add(product);
                    context.SaveChanges();
                    // Response.Redirect("ListProduct.aspx?Id=" + product.Id);
                    Response.Redirect("EditProduct.aspx?Id=" + product.Id);
                    lblResult.Text = "Producto se ha modificado correctamente";
                    lblResult.CssClass = "alert alert-success";

                }
                btnUpdate.Enabled = false;
            }
            catch (Exception ex)
            {
                lblResult.Text = "Error al modificar el producto.";
                lblResult.CssClass = "alert alert-danger";
            }
        }


    protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("ListProduct");
        }

        /// <summary>
        /// Evento que te lleva a el panel del administrador.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnIndex_Click(object sender, EventArgs e)
        {
            Response.Redirect("Index.aspx");
        }
    }
    
}




