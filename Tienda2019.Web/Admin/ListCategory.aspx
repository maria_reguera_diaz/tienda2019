﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListCategory.aspx.cs" Inherits="Tienda2019.Web.Admin.ListCategory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
     <%-- Cargamos el css--%>
    <link href="<%: ResolveUrl("~/Content/DataTables/media/css/jquery.dataTables.css") %>" rel="stylesheet" />
    <h4>Listado de categorías</h4>
    <!-- Creamos tablas para listar los productos-->
     <div style="margin-top:10px">
         <asp:Button ID="btnNew" runat="server" CssClass="btn btn-primary" Text="Nueva categoria" OnClick="btnNew_Click" />
         <asp:Button ID="btnIndex" runat="server" Text="Panel de administracion" style="margin-left:10px" CssClass="btn btn-primary" OnClick="btnIndex_Click" />
     </div>  
    <br />
    
    
    <table id="tbListCategory" class="display" >
        <tr>
             <th>Codigo</th>
             <th>Categoria</th>
             <th>Descripcion </th>
             <th></th>
             <th></th>
        </tr>
        <tbody runat="server" id="tbodyListCategory"></tbody>
    </table>
     <%-- Cargamos el js datatable script--%>
    <script src="<%: ResolveUrl("~/Scripts/DataTables/media/js/jquery.dataTables.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tbListCategory').DataTable({
                "languaje": {
                    "url": "//cdn.datatables.net/plug ins/1.10.13/i18n/Spanish.json"
                }
            });
        });
    </script>
   
</asp:Content>
