﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddAddressUser.aspx.cs" Inherits="Tienda2019.Web.Admin.AddAddressUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <div class="form-horizontal">
        <h4>Añadir Usuario</h4>
        <asp:HiddenField ID="txtId" runat="server" />
        <br />
        <div class="form-row">
            <div class="form-group">
                <asp:Label ID="lblNameUser" runat="server" Text="Nombre" AssociatedControlID="txtNameUser">Nombre</asp:Label>
                <asp:TextBox ID="txtNameUser" runat="server" Text="" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Label ID="lblSurname" runat="server" Text="Apellido" AssociatedControlID="txtSurname">Apellido</asp:Label>
                <asp:TextBox ID="txtSurname" runat="server" Text="" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Label ID="lblStreet" runat="server" AssociatedControlID="txtStreet">Dirección</asp:Label>
                <asp:TextBox ID="txtStreet" Text="" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Label ID="lblNumber" runat="server" AssociatedControlID="txtNumber">Numero</asp:Label>
                <asp:TextBox ID="txtNumber" Text="" CssClass="form-control" runat="server" />
            </div>
            <div class="form-group">
                <asp:Label ID="lblFloor" runat="server" AssociatedControlID="txtFloor">Piso</asp:Label>
                <asp:TextBox ID="txtFloor" Text="" CssClass="form-control" runat="server" />
            </div>
            <div class="form-group">
                <asp:Label ID="lblCity" runat="server" CssClass="control-label" AssociatedControlID="txtCity">Ciudad</asp:Label>
                <asp:TextBox ID="txtCity" runat="server" Text="" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <asp:Label ID="lblProvince" runat="server" CssClass="col-md-3.col-me-offset-2 control-label" Text="Provincia" AssociatedControlID="txtProvince"></asp:Label>
                <asp:TextBox ID="txtProvince" runat="server" CssClass="form-control" />
            </div>
            <div class="form-group">
                <asp:Label ID="lblZip" runat="server" CssClass="control-label" Text="C.P." AssociatedControlID="txtZip"></asp:Label>
                <asp:TextBox ID="txtZip" runat="server" CssClass="form-control" />
            </div>
            <div class="form-group">
                <asp:Label ID="lblCountry" runat="server" CssClass="control-label" Text="País" AssociatedControlID="txtCountry"></asp:Label>
                <asp:TextBox ID="txtCountry" runat="server" CssClass="form-control" />
            </div>
        </div>
    </div>
    <div class="form-horizontal">
        <div class="form-group">
            <asp:Button ID="btnSave" runat="server" Text="Guardar" CssClass="btn btn-success" OnClick="btnSave_Click" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancelar" CssClass="btn btn-warning" />
        </div>
    </div>
    <asp:Label ID="lblResult" runat="server"></asp:Label>
    <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
</asp:Content>
