﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tienda2019.Aplication;
using Tienda2019.CORE;
using Tienda2019.DAL;

namespace Tienda2019.Web.Admin
{
    /// <summary>
    /// Clase para añadir los usuarios por el administrador
    /// </summary>
    public partial class AddAddressUser : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        AddressUserManager addressUserManager = null;
        AddressUser addressUser = null;
        ApplicationUser user = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            int id;
            var addressId = Request.QueryString["Id"];
            if(int.TryParse(addressId, out id))
            {
                addressUser = addressUserManager.GetById(int.Parse(addressId));
                if(addressUser != null)
                {
                    txtNameUser.Text = user.UserName;
                    txtSurname.Text = user.Surname;
                    txtStreet.Text = addressUser.Street;
                    txtNumber.Text = addressUser.Number.ToString();
                    txtFloor.Text = addressUser.Floor.ToString();
                    txtCity.Text = addressUser.City;
                    txtProvince.Text = addressUser.Province;
                    txtZip.Text = addressUser.Code.ToString();
                    txtCountry.Text = addressUser.Country;

                }
            }


            
        }
        /// <summary>
        /// Evento que guarda los usuarios en la base de datos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            addressUserManager = new AddressUserManager(context);
            
            AddressUser addressUser = new AddressUser
            {
                Street = txtStreet.Text,
                Number = int.Parse(txtNumber.Text),
                Floor = int.Parse(txtFloor.Text),
                City = txtCity.Text,
                Province = txtProvince.Text,
                Code = int.Parse(txtZip.Text),
                Country = txtCountry.Text,
                UserId = User.Identity.GetUserId()

            };

            ApplicationUser user = new ApplicationUser()
            {
                NameUser = txtNameUser.Text,
                Surname = txtSurname.Text,
                //UserId = User.Identity.GetUserId()
            };
            

            addressUserManager.Add(addressUser);
            addressUserManager.Context.SaveChanges();
            Response.Redirect("ListUser");
            lblResult.Text = "Dirección de usuario guardada correctamente.";
            lblResult.CssClass = "has-success";
            btnSave.Enabled = false;
        }
        
    }
    
}