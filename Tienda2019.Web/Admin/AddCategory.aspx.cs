﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tienda2019.Application;
using Tienda2019.CORE;
using Tienda2019.DAL;

namespace Tienda2019.Web.Admin
{
    /// <summary>
    /// Clase que añade una categoria a la base de datos.
    /// </summary>
    public partial class AddCategory : System.Web.UI.Page
    {
        ApplicationDbContext context = new ApplicationDbContext();
        //lo primero que se carga
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Botón que guarda la categoria
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            CategoryManager categoryManager = new CategoryManager(context);

            Category category = new Category()
            {
                NameCategory = txtNameCategory.Text,
                DescriptionCategory = txtDescriptionCategory.Text,

            };

            List<Category> list = categoryManager.GetAll().ToList();
            categoryManager.Add(category);
            context.SaveChanges();
            Response.Redirect("ListCategory.aspx");


            // para que se inhabilite el botón y no guardar dos veces.
            btnSave.Enabled = false;
        }
        /// <summary>
        /// Evento que te lleva a el panel del administrador.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnIndex_Click(object sender, EventArgs e)
        {
            Response.Redirect("Index.aspx");
        }
        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("ListCategory");
        }
    }
}