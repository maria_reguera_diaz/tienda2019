﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tienda2019.Application;
using Tienda2019.CORE;
using Tienda2019.DAL;

namespace Tienda2019.Web.Admin
{
    public partial class EditCategory : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        CategoryManager categoryManager = null;
        /// <summary>
        /// Crea una lista con todas las categorías.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                context = new ApplicationDbContext();
                categoryManager = new CategoryManager(context);
                

                string categoryId = Request.QueryString["Id"];
                var categories = categoryManager.GetAll();

                if (categoryId != null)
                {
                    //Cargamos la categoria en el formulario de edición
                    var category = categoryManager.GetById(int.Parse(categoryId));



                    if (!Page.IsPostBack)
                    {
                        txtNameCategory.Text = category.NameCategory;
                        txtDescriptionCategory.Text = category.DescriptionCategory;

                    }//Fin de IsPostback
                }
                else
                {
                    Response.Redirect("ListCategory.aspx");
                }
            }
            catch
            {
                lblResult.Text = "No se ha editado la categoria, intentelo de nuevo.";

            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string categoryId = Request.QueryString["Id"];
                if (categoryId != null)
                {
                    Category category = new Category();
                    category = categoryManager.GetById(int.Parse(categoryId));

                    category.NameCategory = txtNameCategory.Text;
                    category.DescriptionCategory = txtDescriptionCategory.Text;


                    context.SaveChanges();
                    Response.Redirect("ListCategory.aspx?Id=" + category.Id);
                    lblResult.Text = "Categoria modificada con exito.";
                }
            }
            catch
            {
                lblResult.Text = "Error al modificar la categoría.";
            }
        }
        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("ListCategory");
        }
        /// <summary>
        /// Evento que te lleva a el panel del administrador.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnIndex_Click(object sender, EventArgs e)
        {
            Response.Redirect("Index.aspx");
        }
    }
}
