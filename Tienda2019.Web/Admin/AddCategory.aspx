﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddCategory.aspx.cs" Inherits="Tienda2019.Web.Admin.AddCategory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />

    <div class="container">
        <h4>Añadir categorías</h4>

        <asp:ValidationSummary runat="server" CssClass="text-danger" />
        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-md"></div>
                <br />
                
                <asp:Label ID="lblNameCategory" runat="server" CssClass="col-md-2.col-me-offset-2 control-label" Text="Nombre" AssociatedControlID="txtNameCategory"></asp:Label>
                <asp:RequiredFieldValidator ID="valNameCategory" runat="server" ErrorMessage="Debe introducir una Categoría" Text="*" ControlToValidate="txtNameCategory"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtNameCategory" runat="server" CssClass="form-control"></asp:TextBox>
                <asp:Label ID="lblDescriptionCategory" runat="server" CssClass="col-md-2.col-md-offset-2 control-label" Text="Descripción" AssociatedControlID="txtDescriptionCategory"></asp:Label>
                <asp:TextBox ID="txtDescriptionCategory" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="8"></asp:TextBox>
            </div>
            <asp:Button ID="btnSave" runat="server" Text="Guardar" CssClass="btn btn-success" OnClick="btnSave_Click" />
            <asp:Button ID="btnVolver" runat="server" Text="Volver a listado" CssClass="btn btn-warning" OnClick="btnVolver_Click" />
            <asp:Button ID="btnIndex" runat="server" Text="Panel de administracion" style="margin-left:10px" CssClass="btn btn-primary" OnClick="btnIndex_Click" />


            <asp:Label ID="lblResult" runat="server"></asp:Label>
        </div>
    </div>
    
</asp:Content>
