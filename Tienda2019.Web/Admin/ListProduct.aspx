﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListProduct.aspx.cs" Inherits="Tienda2019.Web.Admin.ListProduct" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <%-- Cargamos el css--%>
    <link href="<%: ResolveUrl("~/Content/DataTables/media/css/jquery.dataTables.css") %>" rel="stylesheet" />
    <link href="../Content/DataTables/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <!-- Creamos tablas para listar los productos-->
     <div style="margin-top:10px">
         <asp:Button ID="btnNew" runat="server" Text="Nuevo producto" CssClass="btn btn-primary" OnClick="btnNew_Click" />

         <asp:Button ID="btnIndex" runat="server" Text="Panel de administracion" style="margin-left:10px" CssClass="btn btn-primary" OnClick="btnIndex_Click" />
     </div>  
    <br />
    <table id="tbListProduct">
        
            <tr>
                <!--<th><asp:Button ID="btnEdit" runat="server" CssClass="btn btn-info" /></th>-->
                <th></th>
                <th>Nombre</th>
                <th>Descripcion</th>
                <th>Precio</th>
                <th>Cantidad</th>
                <th>Categoria</th>
                <th></th>
                <th></th>
                <!--<th>Categoria</th>-->
                
            </tr>
       
        <tbody id="tBodyListProduct" runat="server">

    </tbody>
    </table>
   
    <%-- Cargamos el js datatable script--%>
    <script src="<%: ResolveUrl("~/Scripts/DataTables/media/js/jquery.dataTables.js") %>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tbListProduct').DataTable({
                "languaje": {
                    "url": "//cdn.datatables.net/plug ins/1.10.13/i18n/Spanish.json"
                }
            });
        });
    </script>
   <!--<script type="text/javascript">
        $(document).ready(function () {
            $('#tbListProduct').DataTable({
                'bProcessing': true,
                'bServerSide': true,
                'sAjaxSource': '/Admin/ProductServiceList.ashx',
                "olanguaje": {
                    "sUrl": "//cdn.datatables.net/plug ins/1.10.13/i18n/Spanish.json"
                },
                "columns": [
                    
                             { "data": "Product", "Name": "Product", "autoWidth":true },
                             { "data": "Description", "Name": "Description", "autoWidth": true },
                             { "data": "Price", "Name": "Price", "autoWidth": true },
                             { "data": "Quantity", "Name": "Quantity", "autoWidth":true },
                  
                ],
                "columnsDefs":[
                    {
                        "render": function (data, type, row) {
                            return "<a href='Admin/EditProduct?id=" + row.Id + "' class='btn btn-pink'>" + data +"</a>";
                        },
                       
                        "targets": 0

                    },
                    
                ]
            });
        });

    </script>  -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
</asp:Content>
