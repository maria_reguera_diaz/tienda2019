﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tienda2019.Application;
using Tienda2019.CORE;
using Tienda2019.DAL;

namespace Tienda2019.Web.Admin
{
    /// <summary>
    /// Clase para crear los productos por el administrador
    /// </summary>
    public partial class AddProduct : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        ProductManager productManager = null;
        ImageProductManager imageProductManager = null;
        CategoryManager categoryManager = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            productManager = new ProductManager(context);
            categoryManager = new CategoryManager(context);
            imageProductManager = new ImageProductManager(context);
            //carga el drowlists de categorias
            var categories = categoryManager.GetAll();

            if (ddlNameCategory.SelectedItem == null)
            {
                foreach (var category in categories)
                {
                    ListItem listItem = new ListItem(category.NameCategory, category.Id.ToString());
                    ddlNameCategory.Items.Insert(ddlNameCategory.Items.Count, listItem);
                }
            }
        }
        /// <summary>
        /// Evento que guarda los productos en la base de datos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Product product = new Product
                {
                    NameProduct = txtNameProduct.Text,
                    DescriptionProduct = txtDescriptionProduct.Text,
                    CategoryId = int.Parse(ddlNameCategory.SelectedValue),
                    QuantityProduct = int.Parse(txtQuantityProduct.Text),
                    PriceUnitProduct = double.Parse(txtPriceUnitProduct.Text),
                };
                //Para cargar las imagenes.
                if (fuImageProduct.HasFiles)
                {
                    //Creamos la ruta para guardar los archivos
                    if (fuImageProduct.HasFile)
                    {
                        foreach (var file in fuImageProduct.PostedFiles)
                        {
                            imageProductManager = new ImageProductManager(context);
                            String fileExtension = System.IO.Path.GetExtension(file.FileName).ToLower();
                            //Extensión de archivos permitidos.
                            String[] allowedExtensions = { ".png", ".jpg", ".jpeg", ".gif" };
                            if (allowedExtensions.Contains(fileExtension))
                            {

                                file.SaveAs(Server.MapPath("~/ImagenCatalogos/Big") + file.FileName);
                            }
                            ImageProduct imageProduct = new ImageProduct()
                            {
                                ImageName = file.FileName,
                                ProductId = product.Id
                            };
                            imageProductManager.Add(imageProduct);
                            imageProduct = null;
                        }
                        //imageProductManager.Context.SaveChanges();
                    }

                    productManager.Add(product);
                    context.SaveChanges();
                    Response.Redirect("ListProduct");
                    lblResult.Text = "Producto guardado correctamente";
                    lblResult.CssClass = "has-success";
                }
                btnSave.Enabled = false;


                //    string Path = Server.MapPath("~/Images/");
                //    if (!Directory.Exists(Path))
                //        Directory.CreateDirectory(Path);

                //    int filecount = 0;
                //    filecount += 1;


                //    foreach (HttpPostedFile file in fuImageProduct.PostedFiles)
                //    {
                //        ImageProductManager imageProductManager = new ImageProductManager(context);
                //        file.SaveAs(Path + file.FileName);

                //    }

                //    lblStatus.Text = filecount + "Archivos Subidos";
                //}


            }
            catch
            {
                lblResult.Text = "Error al guardar el producto.";
            }
        }
        /// <summary>
        /// Evento que te lleva a el panel del administrador.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnIndex_Click(object sender, EventArgs e)
        {
            Response.Redirect("Index.aspx");
        }

        protected void btnVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("ListProduct");
        }
    }
}
