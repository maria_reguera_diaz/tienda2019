﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Tienda2019.Web.Admin.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="../Content/bootstrap.css" rel="stylesheet" />
    <link href="../Content/maria.css" rel="stylesheet" />
    <script src="../Scripts/bootstrap.min.js"></script>
    <br />
    <div class="container">
        <div class="row">

            <div class="panel panel-primary">

                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class=""></span>Panel Administrador</h3>
                </div>
                <div class="panel-body">
                    <div class="row">

                        <a href="AddProduct.aspx" class="boton boton-rojo boton-lg" style="margin: 10px 0px 10px 0px" role="button">
                            <!--<span class="glyphicon glyphicon-list-alt"></span>-->
                            <br />
                            Productos</a>
                        <a href="AddCategory" class="boton boton-amarillo boton-lg" style="margin: 10px 0px 10px 0px" role="button">
                            <!--<span class="glyphicon glyphicon-bookmark"></span>-->
                            <br />
                            Categorías</a>
                        <a href="#" class="boton boton-azul boton-lg" style="margin: 10px 0px 10px 0px" role="button">
                            <!--<span class="glyphicon glyphicon-signal"></span>-->
                            <br />
                            Usuarios</a>
                        <a href="#" class="boton boton-verde boton-lg" style="margin: 10px 0px 10px 0px" role="button">
                            <!--<span class="glyphicon glyphicon-comment"></span>-->
                            <br />
                            Pedidos</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
