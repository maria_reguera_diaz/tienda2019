﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tienda2019.Aplication;
using Tienda2019.CORE;
using Tienda2019.DAL;

namespace Tienda2019.Web.Admin
{
    public partial class ListUser : System.Web.UI.Page
    {
        ApplicationDbContext context = null;
        AddressUserManager addressUserManager = null;
        //ApplicationUserManager manager = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            addressUserManager = new AddressUserManager(context);
            UserManager<ApplicationUser> userManager =
                    new UserManager<ApplicationUser>(
                        new UserStore<ApplicationUser>(context));


            var ListaddressUser = addressUserManager.GetAll().ToList();

            string formatLinkEdit = "<a class='btn btn-primary' href='EditUser.aspx?Id={0}'>Modificar</a>";
            string formatLinkDelete = "<a class='btn btn-danger' href='RemoveUser.aspx?Id={0}'>Eliminar</a>";

            ApplicationUser user = new ApplicationUser();
            user = null;

            foreach (var addressUser in ListaddressUser)
            {
                var row = new TableRow();
                row.Cells.Add(new TableCell() { Text = addressUser.Id.ToString() });
                row.Cells.Add(new TableCell() { Text = addressUser.UserId.ToString() });
                row.Cells.Add(new TableCell() { Text = addressUser.User.ToString() });
                row.Cells.Add(new TableCell() { Text = addressUser.Street.ToString() });
                row.Cells.Add(new TableCell() { Text = addressUser.Number.ToString() });
                row.Cells.Add(new TableCell() { Text = addressUser.Floor.ToString() });
                row.Cells.Add(new TableCell() { Text = addressUser.Code.ToString() });
                row.Cells.Add(new TableCell() { Text = addressUser.City.ToString() });
                row.Cells.Add(new TableCell() { Text = addressUser.Province.ToString() });
                row.Cells.Add(new TableCell() { Text = addressUser.Country.ToString() });
                row.Cells.Add(new TableCell() { Text = string.Format(formatLinkEdit, addressUser.Id )});
                row.Cells.Add(new TableCell() { Text = string.Format(formatLinkDelete, addressUser.Id )});

                Controls.Add(row);
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddAddressUser.aspx");
        }
    }
}