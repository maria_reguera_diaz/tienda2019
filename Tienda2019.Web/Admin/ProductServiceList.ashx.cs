﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Script.Serialization;
using Tienda2019.Application;
using Tienda2019.DAL;
using Tienda2019.Web.Models;
//paquete nugets instalaod System.Linq.Dinamic
namespace Tienda2019.Web.Admin
{
    /// <summary>
    /// Descripción breve de ProductServiceList "controlador genérico".
    /// para listado de compra de un usuario
    /// </summary>
    public class ProductServiceList : IHttpHandler
    {
        /// <summary>
        /// implementa la interface HttpContext
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            //cuantos registros queremos que muestre
            var iDisplayLength = int.Parse(context.Request["iDisplayLength"]);
            //si se va a la siguiente pagina muestra a partir del siguiente al ultimo
            var iDisplayStart = int.Parse(context.Request["iDisplayStart"]);
            //Lo que ponemos en el campo para buscar
            var sSearch = context.Request["sSearch"];
            //que columna estoy ordenando y si es ascendente o descendente.
            var iSortDir = context.Request["sSortDir_0"];
            var iSortCol = context.Request["iSortCol_0"];
            var sortColum = context.Request.QueryString["mDataProp_" + iSortCol];


            //instanciamos el contexto de AplicationDbContext DAL
            ApplicationDbContext contextdb = new ApplicationDbContext();
            ProductManager productManager = new ProductManager(contextdb);



            //que registros voy a consultar
            #region select
            var allProducts = productManager.GetAll().ToList();

            var products = allProducts
                .Select(p => new ProductListAdmin
                {
                    Id = p.Id,
                    NameProduct = p.NameProduct,
                    DescriptionProduct = p.DescriptionProduct,
                    PriceUnitProduct = p.PriceUnitProduct,
                    QuantityProduct = p.QuantityProduct,
                });
            #endregion
            #region Filter
            if (!string.IsNullOrWhiteSpace(sSearch))
            {
                string where = @"Id.ToString().Contains(@0) ||
                                 NameProduct.ToString().Contains(@0) ||
                                 DescriptionProduct.ToString().Contains(@0) ||
                                 PriceUnitProduct.ToString().Contains(@0)||
                                 QuantityProduct.ToString().Contains(@0)";

                products = products.Where(where, sSearch);
            }
            #endregion
            #region Paginate
            products = products
                        .OrderBy(sortColum + " " + iSortDir)
                        .Skip(iDisplayStart)
                        .Take(iDisplayLength);
            #endregion

            var result = new
            {
                iTotalRecords = allProducts.Count(),
                iTotalDisplayRecords = allProducts.Count(),
                aaData = products
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(result);
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
        }

        /// <summary>
        /// Propiedad que tiene que implementarse, nos lo obliga la interface
        /// </summary>
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}