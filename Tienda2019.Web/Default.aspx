﻿<%@ Page Title="TiendaO" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Tienda2019.Web._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1><%: Title %></h1>
        <p class="lead">TiendaO puede ayudarle a encontrar el artículo perfecto. </p>
        <p>Tienda especializada en la venta de letras O, de todas las formas, tamaños y colores. </p>
        <p>¿Te vas a quedar sin la tuya? </p>
        <p>Producto que marcan la diferencia y con personalidad propia a un precio irresistible.</p>
        <p><a href="Catalogo/Catalogue.aspx" class="btn btn-primary btn-lg">Entar &raquo;</a></p>
    </div>

</asp:Content>
