﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Tienda2019.Web.Client.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
       <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <span class=""></span>Panel de Usuario</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-md-offset-1">
                            <a href="Client/Order.aspx" class="boton boton-rojo boton-lg" style="margin: 20px;" role="button">Pedidos</a>
                            <a href="#" class="boton boton-amarillo boton-lg" style="margin: 20px" role="button">Datos</a>
                            <a href="~/ShoppingCart/ShoppingCart.aspx" class="boton boton-azul boton-lg" style="margin: 20px" role="button">Carrito</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</asp:Content>
