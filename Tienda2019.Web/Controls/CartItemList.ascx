﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CartItemList.ascx.cs" Inherits="Tienda2019.Web.Controls.CartItemList" %>
        <!-- Area de carrito -->
    <div class="container">
        <table id="cart" class="table table-hover table-condensed">
            <thead>
                <tr>
                    <th style="width: 50%">Producto</th>
                    <th style="width: 10%">Precio</th>
                    <th style="width: 8%">Cantidad</th>
                    <th style="width: 22%" class="text-center">Subtotal</th>
                    <th style="width: 10%"></th>
                </tr>
            </thead>
            <tbody>

                <tr>
                    <td data-th="Product">
                        <div class="row">
                            <div class="col-sm-2 hidden-xs">
                                <asp:Image ID="ImageProductUrl" runat="server" Style="height: 45px; width: 45px;" CssClass="img-responsive" />
                            </div>
                            <div class="col-sm-10">
                                <h4 class="nomargin">
                                    <asp:Label ID="txtNameProduct" runat="server" Text=""></asp:Label>Product 1</h4>
                                <p><asp:Label Id="txtDescriptionProduct" runat="server"></asp:Label> Descripción detallada del producto.</p>
                            </div>
                        </div>
                    </td>
                    <td ID="txtPriceUnitProduct" runat="server" >65€</td>
                    <td Id="txtQuantityProduct" runat="server" >
                        <input type="number" class="form-control text-center" value="1">
                    </td>
                    <td data-th="Subtotal" class="text-center" runat="server">1.99</td>
                    <td class="actions" data-th="">
                        <button class="btn btn-info btn-sm"><i class="fa fa-refresh"></i></button>
                        <button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr class="visible-xs">
                    <td class="text-center" id="lblTotal" runat="server"><strong>Total 1.99</strong></td>
                </tr>
                <tr>
                    <td><a href="../Catalogo/Catalogue.aspx" class="btn btn-warning"><i class="fa fa-angle-left"></i>Continúe Comprando</a></td>
                    <td colspan="2" class="hidden-xs"></td>
                    <td class="hidden-xs text-center"><strong>Total $1.99</strong></td>
                    <td><a href="#" class="btn btn-success btn-block">Pagar <i class="fa fa-angle-right"></i></a></td>
                </tr>
            </tfoot>
        </table>
    </div>
            <!-- Fin Area de Carrito-->