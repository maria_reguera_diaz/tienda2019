﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CatalogueProductControl.ascx.cs" Inherits="Tienda2019.Web.Controls.CatalogueProductControl" %>

<!-- Page Content -->

<div class="contenedor">
    <div class="row">
        <div class="product">
            <div class="img-container">
                <asp:Image ID="ImageProductUrl" runat="server" alt="" />
            </div>
            <div class="product-info">
                <div class="product-content">
                    <h1 class="card-title" runat="server">
                        <asp:Label ID="txtNameProduct" runat="server" Text="">Producto</asp:Label>
                    </h1>
                    <p>
                        <asp:Label ID="txtDescriptionProduct" runat="server" Text="">Descripción de los productos</asp:Label>
                    </p>
                    <h5>
                        <asp:Label ID="txtPriceUnitProduct" runat="server" Text=""></asp:Label>
                    </h5>
                    <h6>
                        <asp:Label ID="txtQuantityProduct" runat="server" Text=""></asp:Label>

                    </h6>

                    <div>
                        <asp:Button ID="btnAddCart" runat="server" Text="Al Carrito" OnClick="btnAddCart_Click" CssClass="btn btn-outline-secondary" />
                        <asp:Button ID="btnInfo" runat="server" Text="+ Info" OnClick="btnInfo_Click" CssClass="btn btn-outline-info" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--https://bootsnipp.com/snippets/00mG-->
