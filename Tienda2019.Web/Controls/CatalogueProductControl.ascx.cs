﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tienda2019.Aplication;
using Tienda2019.Application;
using Tienda2019.CORE;
using Tienda2019.DAL;
using Tienda2019.Web.Models;

namespace Tienda2019.Web.Controls
{
    public partial class CatalogueProductControl : System.Web.UI.UserControl
    {

        public ApplicationDbContext context = null;
        public ImageProductManager imageProductManager = null;
        public ProductManager productManager = null;
        public CategoryManager categoryManager = null;
        public CartItemManager cartItemManager = null;

        /// <summary>
        /// Propiedad publica de producto
        /// </summary>
        public Product Product { get; set; }
        /// <summary>
        /// Propiedad publica de imagen
        /// </summary>
        public ImageProduct ImageProduct { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            imageProductManager = new ImageProductManager(context);
            productManager = new ProductManager(context);
            categoryManager = new CategoryManager(context);
            cartItemManager = new CartItemManager(context);

            txtNameProduct.Text = Product.NameProduct;
            txtPriceUnitProduct.Text = Product.PriceUnitProduct.ToString("C");
            txtQuantityProduct.Text = Product.QuantityProduct.ToString();
            txtDescriptionProduct.Text = Product.DescriptionProduct;



            var imageProducts = imageProductManager.GetAll()
                    .Include(p => p.Product)
                    .Where(p => p.ProductId == Product.Id);
            if (imageProducts != null)
            {
                foreach (var imageProduct in imageProducts)
                {
                    ImageProductUrl.ImageUrl = "~/ImagenCatalogos/Big/" + imageProduct.ImageName;
                }
            }
            if (Product.QuantityProduct < 1)
            {
                txtQuantityProduct.Text = "Agotado";
            }else if (Product.QuantityProduct < 10)
            {
                txtQuantityProduct.Text = "En Stock";// + Product.QuantityProduct.ToString();
            }
           
        }

        protected void btnAddCart_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ShoppingCart/AddToCart.aspx?productId=" + Product.Id);
        }
        protected void btnInfo_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Catalogo/ProductDetail.aspx?productId=" + Product.Id);
        }
    }
}


