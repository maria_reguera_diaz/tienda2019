﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda2019.CORE;

namespace Tienda2019.Web.Models
{
    /// <summary>
    /// Clase de dominio del producto
    /// </summary>
    public class ProductListAdmin
    {
        /// <summary>
        /// Identificador de producto.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Identificador de la categoria a la que pertenece el producto.
        /// </summary>
        [ForeignKey("Category")]
        public int CategoryId { get; set; }

        //public Category Category { get; set; }

        /// <summary>
        /// Nombre del producto.
        /// </summary>
        public string NameProduct { get; set; }

        /// <summary>
        /// Descripción del producto.
        /// </summary>
        public string DescriptionProduct { get; set; }

        /// <summary>
        /// Precio del producto.
        /// </summary>
        public double PriceUnitProduct { get; set; }

        /// <summary>
        /// Cantidad de producto que queda en stock, solo lo puede ver el administrador.
        /// </summary>
        public int QuantityProduct { get; set; }


        /// <summary>
        /// Listado de Imagenes de un producto.
        /// </summary>
        //public List<ImageProduct> ImageProducts { get; set; }
        public string ImageProduct { get; set; }
    }
}
