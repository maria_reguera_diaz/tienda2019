﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda2019.CORE;

namespace Tienda2019.Web.Models
{
    /// <summary>
    /// Clase de dominio del producto solo con los datos que queremos mostrar.
    /// </summary>
    public class ProductListUser
    {
        /// <summary>
        /// Identificador de producto.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Identificador de la categoria a la que pertenece el producto.
        /// </summary>
        [ForeignKey("Category")]
        public int CategoryId { get; set; }

        //public Category Category { get; set; }

        /// <summary>
        /// Nombre del producto.
        /// </summary>
        public string NameProduct { get; set; }

        /// <summary>
        /// Descripción del producto.
        /// </summary>
        public string DescriptionProduct { get; set; }

        /// <summary>
        /// Precio del producto.
        /// </summary>
        public double PriceUnitProduct { get; set; }
       
        /// <summary>
        /// Estado de stock del producto, 
        /// </summary>
        public StockProduct Stock { get; set; }

        /// <summary>
        /// Listado de Imagenes de un producto.
        /// </summary>
        public string ImageProduct { get; set; }

    }
}
