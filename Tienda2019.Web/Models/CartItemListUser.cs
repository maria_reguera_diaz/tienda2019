﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Tienda2019.CORE;

namespace Tienda2019.Web.Models
{
    public class CartItemListUser
    {
        /// <summary>
        /// Identificador de carrito de la compra
        /// </summary>
        [Key]
        public string ItemId { get; set; }
        /// <summary>
        /// Cantidad de productos en el carrito
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// Fecha de creación del carrito.
        /// </summary>
        public DateTime DateCreate { get; set; }
        /// <summary>
        /// Identificador del producto al que pertenece el carrito
        /// </summary>
        [ForeignKey("Product")]
        public int ProductId { get; set; }
        /// <summary>
        /// Colección de producto al que pertence el carrito.
        /// </summary>
        public virtual Product Product { get; set; }
        /// <summary>
        /// Identificador de usuario que ha creado el carrito.
        /// </summary>
        [ForeignKey("User")]
        public String UserId { get; set; }
        public ApplicationUser User { get; set; }
        /// <summary>
        /// Identificador de usuario asociado al articulo a comprar.
        /// </summary>
        public string CardId { get; set; }
    }
}