﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tienda2019.CORE;

namespace Tienda2019.DAL
{
    //Enable-Migrations //Crea la base de datos
    //Add-Migration "Init" // Añade
    //Update-Database -StartUpProjectName "Tienda2019.Web" -ConnectionStringName "DefaultConnection" -Verbose
    //Update-Database -StartUpProjectName "Tienda2019.Web" -ConnectionStringName "AppHarbor" -Verbose
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        /// <summary>
        /// Colección persistible de productos //Clases que queremos que se persistan
        /// </summary>
        public DbSet<Product> Products { get; set; }
        /// <summary>
        /// Colección persistible de categorias
        /// </summary>
        public DbSet<Category> Categories { get; set; }
        /// <summary>
        /// Colección persistible de imágenes
        /// </summary>
        public DbSet<ImageProduct> ImageProducts { get; set; }
        /// <summary>
        /// Colección persistible de carrito que se crearán en la base de datos.
        /// </summary>
        public DbSet<CartItem> CartItems { get; set; }

        /// <summary>
        /// Colección persistible de pedidos que se crearán en la base de datos.
        /// </summary>
        public DbSet<Order> Orders { get; set; }

        /// <summary>
        /// Colección persistible de detalles de pedido que se crearán en la base de datos.
        /// </summary>
        public DbSet<OrderLine> OrderLines { get; set; }
        /// <summary>
        /// Colección persistible de direcciones de usuarios que se crearán en la base de datos.
        /// </summary>
        public DbSet<AddressUser> AddressUsers { get; set; }
        /// <summary>
        /// Colección persistible de envios de usuarios que se crearán en la base de datos.
        /// </summary>
        public DbSet<Shipment> Shipments { get; set; }
        /// <summary>
        /// Colección persistible de metodo de pago que se crearán en la base de datos.
        /// </summary>
        public DbSet<TPV> TPVs { get; set; }
    }
}
