namespace Tienda2019.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class delete : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderLines", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.OrderLines", new[] { "UserId" });
            DropColumn("dbo.OrderLines", "UserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OrderLines", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.OrderLines", "UserId");
            AddForeignKey("dbo.OrderLines", "UserId", "dbo.AspNetUsers", "Id");
        }
    }
}
