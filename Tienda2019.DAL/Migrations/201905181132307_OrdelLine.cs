namespace Tienda2019.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrdelLine : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderLines", "PriceUnitProduct", c => c.Double(nullable: false));
            DropColumn("dbo.OrderLines", "PriceTotalProduct");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OrderLines", "PriceTotalProduct", c => c.Double(nullable: false));
            DropColumn("dbo.OrderLines", "PriceUnitProduct");
        }
    }
}
